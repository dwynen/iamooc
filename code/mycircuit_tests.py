import pytest
import math
from numpy.testing import assert_almost_equal

from myinterval import Interval, INF, NAN
from mycircuit import contract_sum, contract_mul, contract_sqr


def intervals_almost_equal(x, y, decimal=5):
    assert_almost_equal(x.a, y.a, decimal=decimal)
    assert_almost_equal(x.b, y.b, decimal=decimal)


def test_cadd():
    a = Interval(-1, 2)
    b = Interval(3, 4)
    c = Interval(5, 20)

    result = contract_sum(a, b, c)
    expected = Interval(1, 2), Interval(3, 4), Interval(5, 6)
    assert expected == result


def test_cmul():
    a = Interval(-1, 2)
    b = Interval(3, 4)
    c = Interval(5, 20)

    result = contract_mul(a, b, c)
    expected = Interval(1.25, 2), Interval(3, 4), Interval(5, 8)
    assert expected == result


def test_csqr():
    a = Interval(-1, 2)
    b = Interval(3, 4)

    result = contract_sqr(a, b)
    expected = Interval(1.732051, 2), Interval(3, 4)
    assert len(result) == len(expected)
    for x, y in zip(result, expected):
        intervals_almost_equal(x, y)
