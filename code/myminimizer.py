from myinterval import Interval, INF, NAN
from vibes import vibes

import numpy as np


def draw_bounds(f, x_min, x_max, dx, color="red"):
    interval_boundaries = np.arange(x_min, x_max, dx)
    if interval_boundaries[-1] < x_max:
        interval_boundaries = np.append(interval_boundaries, [x_max])

    min_values = Interval(INF, INF)
    for x1, x2 in zip(interval_boundaries[:-1], interval_boundaries[1:]):
        x12 = Interval(x1, x2)
        fx12 = f(x12)
        min_values = min_values.min(fx12)
        vibes.drawBox(x1, x2, fx12.a, fx12.b, color)

    return min_values


def example_function(x):
    return x**2 + 2 * x - x.exp()


def main():
    vibes.beginDrawing()
    vibes.newFigure("Function bounds")
    vibes.setFigureProperties({"x": 0, "y": 0, "width": 768, "height": 768})
    vibes.axisLimits(-3, 4, -3, 4)
    params = {
        "[blue]": 0.5,
        "[red]": 0.05,
        "[yellow]": 0.005,
        "[green]": 0.0005,
    }
    for color, value in params.items():
        min_values = draw_bounds(example_function, -2, 2, value, color)
        print(min_values)


if __name__ == "__main__":
    main()
