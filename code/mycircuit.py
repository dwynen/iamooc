# needed to annotate ContractorPlaceholder within ContractorPlaceholder
from __future__ import annotations

from myinterval import Interval, INF, NAN
from mybox import Box
from contractors import contract_sum, contract_mul, contract_sqr, relative_change


def main(E, I, U1, U2, P):

    U = Interval(-INF, INF)
    R = Interval(-INF, INF)
    # not ruling out that these resistors are really sources in disguise
    R1 = Interval(-INF, INF)
    R2 = Interval(-INF, INF)
    Isqr = Interval(0, INF)

    while True:
        E_, I_, U1_, U2_, P_, U_, R_, R1_, R2_, Isqr_ = (
            E,
            I,
            U1,
            U2,
            P,
            U,
            R,
            R1,
            R2,
            Isqr,
        )
        # U1 = R1 * I
        R1_, I_, U1_ = contract_mul(R1_, I_, U1_)
        # U2 = R2 * I
        R2_, I_, U2_ = contract_mul(R2_, I_, U2_)
        # R = R1 + R2
        R1_, R2_, R_ = contract_sum(R1_, R2_, R_)
        # P = E * I
        E_, I_, P_ = contract_mul(E_, I_, P_)
        # E = U1 + U2
        U1_, U2_, E_ = contract_sum(U1_, U2_, E_)
        # U = R * I
        I_, R_, U_ = contract_mul(I_, R_, U_)
        # P = R * I**2
        I_, Isqr_ = contract_sqr(I_, Isqr_)
        R_, Isqr_, P_ = contract_mul(R_, Isqr_, P_)

        max_change = max(
            relative_change(before, after)
            for before, after in zip(
                # fmt: off
            (E,  I,  U1,  U2,  P,  U,  R,  R1,  R2,  Isqr,),
            (E_, I_, U1_, U2_, P_, U_, R_, R1_, R2_, Isqr_,),
                # fmt: on
            )
        )
        if max_change > 0.01:
            E, I, U1, U2, P, U, R, R1, R2, Isqr = (
                E_,
                I_,
                U1_,
                U2_,
                P_,
                U_,
                R_,
                R1_,
                R2_,
                Isqr_,
            )
            continue
        else:
            return R1_, R2_


if __name__ == "__main__":
    E = Interval(23, 26)
    I = Interval(4, 8)
    U1 = Interval(10, 11)
    U2 = Interval(14, 17)
    P = Interval(124, 130)
    print(main(E, I, U1, U2, P))
