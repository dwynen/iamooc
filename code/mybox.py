import numpy as np
from myinterval import Interval, INF, NAN


class Box:
    def __init__(self, *args):
        self.intervals = [Interval(x) for x in args]

    def __repr__(self):
        strs = [str(x) for x in self.intervals]
        return f"Box({', '.join(strs)})"

    @property
    def ndim(self):
        return len(self.intervals)

    @property
    def width(self):
        """
        Returns width of widest interval, and which dimension it belongs to.
        """
        widths = [i.width for i in self.intervals]
        max_dim = np.argmax(widths)
        max_width = widths[max_dim]
        return max_width, max_dim

    def split_along_width(self):
        width, max_dim = self.width
        split_dim = self.intervals[max_dim]
        # make copies of my own intervals
        left_half = list(self.intervals)
        right_half = list(self.intervals)
        # replace the interval in question
        left_half[max_dim] = Interval(split_dim.a, split_dim.a + (width / 2))
        right_half[max_dim] = Interval(split_dim.a + (width / 2), split_dim.b)
        return Box(*left_half), Box(*right_half)

    @property
    def corners(self):
        res = list()
        for i in self.intervals:
            res.extend([i.a, i.b])
        return res

    def __getitem__(self, key):
        return self.intervals[key]

    def __and__(self, other):
        assert self.ndim == other.ndim, (
            "Trying to intersect boxes of differing dimensions "
            f"{self.ndim} and {other.ndim}"
        )
        return Box(*(x & y for (x, y) in zip(self.intervals, other.intervals)))

    def __or__(self, other):
        assert self.ndim == other.ndim, (
            "Trying to union boxes of differing dimensions "
            f"{self.ndim} and {other.ndim}"
        )
        return Box(*(x | y for (x, y) in zip(self.intervals, other.intervals)))

    def __bool__(self):
        return all(self.intervals)
