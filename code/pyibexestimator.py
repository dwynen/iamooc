#!/usr/bin/env python3

import time
import argparse
import functools

import numpy as np
from vibes import vibes
import pyibex as ibex

# 🦇 🦇 🦇 pretty colors 🦇 🦇 🦇
import dracula

PARAM_ESTIMATION = "Parameter estimation with ibex"


def parse_args():
    parser = argparse.ArgumentParser("Parameter estimator for exercise 9")
    parser.add_argument(
        "-i",
        "--measurement",
        type=int,
        help="If given, only draw the parameter set for this one measurement",
    )
    parser.add_argument(
        "-q",
        "--accept-outliers",
        type=int,
        help="If given, the chosen number of measurements may be in conflict with the parameters. Does not combine with -q.",
    )
    parser.add_argument(
        "-w",
        "--split-threshold",
        type=float,
        default=0.01,
        help=(
            "Boxes with a width smaller than this will not be split anymore and "
            'just be displayed as "unknown"'
        ),
    )
    parser.add_argument(
        "-l",
        "--long-measurements",
        action="store_true",
        help="If given, append three more measurments, two of which are clearly outliers.",
    )

    parser.add_argument(
        "-g",
        "--gomne",
        action="store_true",
        help="If given, run GOMNE for each input measurement, checking whether it should be regarded as an outlier.",
    )

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()


def main(args):
    measurements = [
        [0.2, ibex.Interval(1.5, 2)],
        [1, ibex.Interval(0.7, 0.8)],
        [2, ibex.Interval(0.1, 0.3)],
        [4, ibex.Interval(-0.1, 0.03)],
    ]
    if args.long_measurements:
        measurements += [
            [5, ibex.Interval(-0, 0.02)],
            [6, ibex.Interval(0.5, 1)],
            [7, ibex.Interval(1, 2)],
        ]
    assert (
        args.accept_outliers is None or args.measurement is None
    ), "Can't handle both -i and -q"
    if args.measurement is not None:
        measurements = [measurements[args.measurement]]
    vibes.beginDrawing()

    X0 = ibex.IntervalVector([[-3, 3], [-3, 3]])
    vibes.newFigure(PARAM_ESTIMATION)
    vibes.setFigureProperties({"x": 200, "y": 100, "width": 768, "height": 768})
    vibes.drawBox(*corners(4 * X0), color=f"[{dracula.BACKGROUND}]")
    vibes.axisEqual()
    vibes.axisLimits(*corners(X0))

    separators = [
        ibex.SepFwdBwd(ibex.Function("p1", "p2", f"p1 * exp(p2 * {t})"), y)
        for t, y in measurements
    ]
    separator = ibex.SepQInterProjF(separators)
    if args.gomne:
        assert args.accept_outliers is None, "Can't set q and also estimate it"
        q, w = gomne(
            separator,
            X0,
            epsilon_0=args.split_threshold,
            max_q=len(measurements),
        )
        print(f"Accepting {q} outliers produces a non-empty solution set.")
    else:
        q = args.accept_outliers
        w = args.split_threshold

    if q:
        separator.q = q
    ibex.pySIVIA(
        X0,
        separator,
        w,
        color_maybe=f"{dracula.CURRENT}[{dracula.GREEN}]",
        color_in=f"{dracula.CURRENT}[{dracula.PURPLE}]",
        color_out=f"{dracula.CURRENT}",
    )


def gomne(
    separator,
    X0,
    epsilon_0,
    max_q,
    min_epsilon=1e-4,
):
    """
    Very simple implementation of the GOMNE algorithm:
    https://hal.archives-ouvertes.fr/hal-00845581
    """
    q = 0
    while q <= max_q:
        epsilon = epsilon_0
        while epsilon >= min_epsilon:
            separator.q = q
            res_in, res_out, res_y = ibex.pySIVIA(
                X0, separator, epsilon, draw_boxes=False
            )
            if res_in or (not res_in and not res_y):
                break
            epsilon /= 2
        if res_in:
            return q, epsilon
        q += 1
    raise Exception(
        "wtf, couldn't find even a single valid parameter set for even one measurement?!"
    )


def corners(box: ibex.IntervalVector) -> tuple[float, float, float, float]:
    return box[0].lb(), box[0].ub(), box[1].lb(), box[1].ub()


if __name__ == "__main__":
    main(args)
