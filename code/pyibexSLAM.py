#!/usr/bin/env python3

import time

import numpy as np

from vibes import vibes
import pyibex as ibex

# 🦇 🦇 🦇 pretty colors 🦇 🦇 🦇
import dracula


def sample_noise(i: ibex.Interval) -> float:
    return i.lb() + np.random.random() * i.diam()


def move_robot(
    true_x: float,
    true_y: float,
    true_theta: float,
    v: float,
    u_theta: float,
    dt: float,
    n_u: ibex.Interval,
):
    next_x = true_x + dt * v * np.cos(true_theta)
    next_y = true_y + dt * v * np.sin(true_theta)
    next_theta = true_theta + dt * (u_theta + sample_noise(n_u))

    return next_x, next_y, next_theta


def corners(box: ibex.IntervalVector) -> tuple[float, float, float, float]:
    return box[0].lb(), box[0].ub(), box[1].lb(), box[1].ub()


def contract_state(
    x: ibex.Interval,
    y: ibex.Interval,
    x1: ibex.Interval,
    y1: ibex.Interval,
    theta: ibex.Interval,
    v: float,
    dt: float,
) -> tuple[float, float, float]:
    fx = ibex.Function("x1", "x", "theta", f"x1 - x - {dt} * {v} * cos(theta)")
    fy = ibex.Function("y1", "y", "theta", f"y1 - y - {dt} * {v} * sin(theta)")

    # TODO: replace [0, 0] with something a bit more lenient
    Cx = ibex.CtcFwdBwd(fx, ibex.Interval(0, 0))
    Cy = ibex.CtcFwdBwd(fy, ibex.Interval(0, 0))

    x_box = ibex.IntervalVector([x1, x, theta])
    Cx.contract(x_box)
    x1, x, theta = x_box
    y_box = ibex.IntervalVector([y1, y, theta])
    Cy.contract(y_box)
    y1, y, theta = y_box

    return x1, y1


def contract_landmark_measurement(
    xk: ibex.Interval,
    yk: ibex.Interval,
    landmark_previous_estimate: ibex.IntervalVector,
    d: ibex.Interval,
):
    f = ibex.Function("x", "y", "lx", "ly", "(x - lx)^2 + (y - ly)^2")
    C = ibex.CtcFwdBwd(f, ibex.sqr(d))
    X = ibex.IntervalVector([xk, yk, *landmark_previous_estimate])
    C.contract(X)
    xk, yk, lx, ly = X
    return xk, yk, ibex.IntervalVector([lx, ly])


if __name__ == "__main__":
    LANDMARKS_true = [
        [6, 1],
        [-2, -5],
        [-3, 4],
        [3, 4],
        [5, -8],
        [5, -16],
        [-15, -8],
        [-10, 0],
        [2, -1],
    ]
    NUM_LANDMARKS = 4
    USE_LANDMARKS = True
    K_MAX = 200
    SEE_CLOSEST_LANDMARK = False

    LANDMARKS_PER_STEP = 1
    SLEEP = 0.05

    # time step
    dt = 0.1

    # speed. constant because that's how we... roll :)
    v = 10

    # The true landmarks are not known to the robot but will be useful for simulation &
    # drawing
    LANDMARKS_true = np.asarray(LANDMARKS_true[:NUM_LANDMARKS], dtype=np.float64)

    # measurement noise of the heading
    n_theta = ibex.Interval(-0.03, 0.03)

    # the control of the heading also has some noise attached this happens to be the
    # same noise here, but don't rely on that
    n_u = ibex.Interval(-0.03, 0.03)

    # noise for the landmark distance measurement
    n_d = ibex.Interval(-0.03, 0.03)

    vibes.beginDrawing()

    X0 = ibex.IntervalVector([[-20, 20], [-20, 20]])

    vibes.newFigure("SLAM!!!!1!!11!eleven")
    vibes.drawBox(*corners(4 * X0), color=f"[{dracula.BACKGROUND}]")
    vibes.setFigureProperties({"x": 300, "y": 20, "width": 1024, "height": 1024})
    vibes.axisEqual()
    vibes.axisLimits(*corners(X0))

    # draw the true landmark positions for reference
    if USE_LANDMARKS:
        for l in LANDMARKS_true:
            vibes.drawCircle(l[0], l[1], 0.2, color=f"{dracula.RED}[{dracula.RED}aa]")

    ###################################################################################
    # this is the true state. obviously not visible to the robot, but useful for us :)
    ###################################################################################
    x_true = 0
    y_true = 0
    theta_true = 1

    ###################################################################################
    # These are things that the robot does see
    ###################################################################################
    # for t = 0 we know the state
    x_est = ibex.Interval(0)
    y_est = ibex.Interval(0)
    theta_est = ibex.Interval(1)
    # the landmark estimates are initialized to "we don't know"
    landmarks = [ibex.IntervalVector(2) for _ in range(LANDMARKS_true.shape[0])]
    ###################################################################################

    for k in range(1, K_MAX + 1):
        ###############################################################################
        # simulate what actually happens with the robot
        u_k = 3 * np.sin(k * dt) ** 2
        x_true, y_true, theta_true = move_robot(
            x_true, y_true, theta_true, v, u_k, dt, n_u
        )
        ###############################################################################

        x_est, y_est = contract_state(
            x_est,
            y_est,
            ibex.Interval.ALL_REALS,
            ibex.Interval.ALL_REALS,
            theta_est,
            v=v,
            dt=dt,
        )

        if USE_LANDMARKS:
            for i in range(1 if SEE_CLOSEST_LANDMARK else LANDMARKS_PER_STEP):
                # also every time step, we get to measure the distance to a landmark
                if SEE_CLOSEST_LANDMARK:
                    l_idx = np.argmin(
                        np.linalg.norm(LANDMARKS_true - [x_true, y_true], axis=1)
                    )
                else:
                    l_idx = np.random.choice(LANDMARKS_true.shape[0])
                # the measurement has added noise n_d though, which we also simulate here
                d_true = np.linalg.norm(
                    LANDMARKS_true[l_idx] - [x_true, y_true]
                ) + sample_noise(n_d)

                # but we know that our measurement is noisy
                d = d_true + n_d
                x_est, y_est, landmarks[l_idx] = contract_landmark_measurement(
                    x_est, y_est, landmarks[l_idx], d
                )
                vibes.drawBox(
                    *corners(landmarks[l_idx]),
                    color=f"{dracula.PINK}05[{dracula.PINK}05]",
                )

        # we're only keeping the updated estimate of the _new_ state. We could also
        # update the old state, but that'd kinda be cheating. After all, back then we
        # didn't know better. Why would we pretend now that we did?
        vibes.drawBox(
            x_est.lb(),
            x_est.ub(),
            y_est.lb(),
            y_est.ub(),
            color=f"{dracula.COMMENT}20[{dracula.COMMENT}20]",
        )

        # draw true state on top of everything else to see how well we did
        vibes.drawAUV(
            x_true,
            y_true,
            np.degrees(theta_true),
            0.7,
            color=f"{dracula.COMMENT}[{dracula.CYAN}]",
        )

        # every time step we make a new estimate of theta just by measuring with a
        # compass. however, again a random realization of the measurement noise is added
        theta_est = theta_true + sample_noise(n_theta) + n_theta

        if SLEEP > 0:
            time.sleep(SLEEP)
