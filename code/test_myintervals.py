import pytest
import math
from numpy.testing import assert_almost_equal

from myinterval import Interval, INF, NAN


def intervals_almost_equal(x, y, decimal=7):
    assert_almost_equal(x.a, y.a, decimal=decimal)
    assert_almost_equal(x.b, y.b, decimal=decimal)


## My own tests and those from the exercise video {{{


def test_add():
    x = Interval(4, 5)
    y = Interval(3, 4)
    assert x + y == Interval(7, 9)


def test_sub():
    x = Interval(4, 5)
    y = Interval(3, 4)
    assert x - y == Interval(0, 2)


def test_mul():
    x = Interval(4, 5)
    y = Interval(3, 4)
    assert x * y == Interval(12, 20)


def test_rmul():
    x = Interval(4, 5)
    assert 3 * x == Interval(12, 15)


def test_lmul():
    x = Interval(4, 5)
    assert x * 4 == Interval(16, 20)


def test_neg():
    x = Interval(4, 5)
    assert -x == Interval(-5, -4)


def test_truediv():
    x = Interval(4, 5)
    y = Interval(3, 4)
    z = Interval(-100, 1_000_000)
    intervals_almost_equal(x / y, Interval(1, 5 / 3))
    intervals_almost_equal(x / z, Interval(-INF, INF))


def test_exp():
    x = Interval(0, 1)
    intervals_almost_equal(x.exp(), Interval(1, math.e))


def test_log_both_neg():
    x = Interval(-2, 0)
    intervals_almost_equal(x.log(), Interval.empty_interval())


def test_log_half_neg():
    x = Interval(0, 1)
    intervals_almost_equal(x.log(), Interval(-INF, 0))


def test_log_both_neg():
    x = Interval(1, math.e)
    intervals_almost_equal(x.log(), Interval(0, 1))


def test_sqr():
    x = Interval(4, 5)
    intervals_almost_equal(x.sqr(), Interval(16, 25))


def test_pow_2_neg_neg():
    x = Interval(-4, -3)
    intervals_almost_equal(x**2, Interval(9, 16))


def test_pow_2_neg_pos():
    x = Interval(-4, +3)
    intervals_almost_equal(x**2, Interval(0, 16))


def test_pow_3_neg_neg():
    x = Interval(-4, -3)
    intervals_almost_equal(x**3, Interval(-64, -27))


def test_pow_3_neg_pos():
    x = Interval(-4, +3)
    intervals_almost_equal(x**3, Interval(-64, 27))


def test_pow_0_neg_pos():
    x = Interval(-4, +3)
    intervals_almost_equal(x**0, Interval(1, 1))


def test_pow_minus_1_neg_pos():
    x = Interval(-4, +3)
    intervals_almost_equal(x**-1, Interval(-1 / 4, 1 / 3))


def test_min():
    x = Interval(4, 5)
    y = Interval(3, 4)
    assert x.min(y) == Interval(3, 4)


def test_max():
    x = Interval(4, 5)
    y = Interval(3, 4)
    assert x.max(y) == Interval(4, 5)


# }}}

# Tests from Exercise 1-4 {{{


@pytest.fixture
def x():
    return Interval(-1, 3)


@pytest.fixture
def y():
    return Interval(2, 5)


def test_ex1_1(x, y):
    assert x + y == Interval(1, 8)
    assert x - y == Interval(-6, 1)
    assert x * y == Interval(-5, 15)
    intervals_almost_equal(x / y, Interval(-1 / 2, 3 / 2))
    assert x.max(y) == Interval(2, 5)
    assert x.min(y) == Interval(-1, 3)


def test_ex1_2():
    assert Interval(-2, 4) * Interval(1, 3) == Interval(-6, 12)
    # didn't implement set operations
    # assert Interval(-2, 4) * Interval(6, 7) == Interval(-6, 12)
    assert Interval(2, 7).max(Interval(1, 9)) == Interval(2, 9)
    assert Interval.empty_interval().max(Interval(1, 1)) == Interval.empty_interval()
    intervals_almost_equal(Interval(-1, 3) / Interval(0, INF), Interval(-INF, INF))
    # again, not doing this since I didn't implement the set operations
    # assert (Interval(1, 2) * Interval(-1, 3)) + (Interval(1, 3).intersect(Interval(6, 7))).max(Interval(1, 2)) == Interval.empty_interval()


def test_ex1_3():
    assert Interval(-1, 3) ** 2 == Interval(0, 9)
    assert Interval(-10, 4).sqrt() == Interval(0, 2)
    assert Interval(-2, -1).log() == Interval.empty_interval()


def test_ex1_4():
    assert (Interval(1, 2) + Interval(-3, 4)) * Interval(1, 5) == Interval(-10, 30)
    assert (Interval(1, 2) / Interval(0, INF)).exp() == Interval(1, INF)


# }}}


def test_benchmark_input():
    x = Interval(-2, 2)
    result = x**2 + 2 * x - x.exp()
    # only comparing up to 6 digits here because that's the comparison value
    # from the solution video. But I guess if there was an actual error in my
    # implementation it would show up as more than a rounding error. :)
    intervals_almost_equal(result, Interval(-11.389056, 7.864665), decimal=6)
