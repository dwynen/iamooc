#!/usr/bin/env python3

import math

NAN = float("nan")
INF = float("inf")


class Interval:
    @staticmethod
    def empty_interval():
        return Interval(NAN, NAN)

    @staticmethod
    def real():
        return Interval(-INF, INF)

    def __init__(self, a, b=None):
        if isinstance(a, list) or isinstance(a, tuple):
            assert (
                b is None
            ), f"When initializing from list or tuple, b must be None. Got {b}."
            assert (
                len(a) == 2
            ), f"When initializing from list or tuple, len(a) must be 2. Got {len(a)}"
            a, b = a[0], a[1]
        elif isinstance(a, Interval):
            assert (
                b is None
            ), f"When initializing from Interval, b must be None. Got {b}."
            a, b = a.a, a.b

        # this allows initializing a singular interval [a, a]
        if b is None and not isinstance(a, Interval):
            b = a

        if a > b:
            self.a, self.b = NAN, NAN
        else:
            self.a, self.b = a, b

    def __eq__(self, other):
        if math.isnan(self.a) or math.isnan(self.b):
            return math.isnan(other.a) and math.isnan(other.b)
        return self.a == other.a and self.b == other.b

    def __neq__(self, other):
        return self.a != other.a or self.b != other.b

    def __add__(self, other):
        return Interval(self.a + other.a, self.b + other.b)

    def __sub__(self, other):
        return Interval(self.a - other.b, self.b - other.a)

    def __mul__(self, other):
        if not isinstance(other, Interval):
            return other * self
        aa = self.a * other.a
        ab = self.a * other.b
        ba = self.b * other.a
        bb = self.b * other.b
        return Interval(min([aa, ab, ba, bb]), max([aa, ab, ba, bb]))

    def __radd__(self, other):
        other_interval = Interval(other, other)
        return self.__add__(other_interval)

    def __rmul__(self, other):
        other_interval = Interval(other, other)
        return self.__mul__(other_interval)

    def __truediv__(self, other):
        # Theres's a special case here for when 0 is one of the bounds, so
        # can only be approached from the top, but not the bottom
        if 0 in other and 0 not in [other.a, other.b]:
            return Interval(-INF, INF)
        return self * Interval(
            1 / other.b if other.b != 0 else -INF,
            1 / other.a if other.a != 0 else INF,
        )

    def __pos__(self):
        return self

    def __neg__(self):
        return Interval(-self.b, -self.a)

    def __contains__(self, a):
        return self.a <= a <= self.b

    def __pow__(self, p):
        if int(p) != p:
            raise ValueError("Non-integer power is not supported.")
        results = self.a**p, self.b**p
        if p != 0 and p % 2 == 0 and 0 in self:
            return Interval(0, max(results))
        return Interval(min(results), max(results))

    def __bool__(self):
        if math.isnan(self.a) or math.isnan(self.b):
            assert math.isnan(self.a) and math.isnan(
                self.b
            ), f"got inconsistent interfal {self}"
            return False
        assert self.a <= self.b, f"got inconsistent interval {self}"
        return True

    def sqr(self):
        """
        Special case of power operator. Demanded in exercise, so implemented here
        for clarity.
        """
        return self**2

    def __repr__(self):
        return f"[{self.a}, {self.b}]"

    def __and__(self, other):
        if not self or not other:
            return Interval.empty_interval()
        return Interval(max(self.a, other.a), min(self.b, other.b))

    def __or__(self, other):
        if not self:
            return other
        if not other:
            return self
        return Interval(min(self.a, other.a), max(self.b, other.b))

    def sqrt(self):
        return Interval(math.sqrt(max(self.a, 0)), math.sqrt(max(self.b, 0)))

    def exp(self):
        return Interval(math.exp(self.a), math.exp(self.b))

    def log(self):
        if self.b <= 0:
            return Interval.empty_interval()
        if 0 in self:
            return Interval(-INF, math.log(self.b))
        return Interval(math.log(self.a), math.log(self.b))

    def min(self, other):
        results = [
            min(self.a, other.a),
            min(self.a, other.b),
            min(self.b, other.a),
            min(self.b, other.b),
        ]
        return Interval(min(results), max(results))

    def max(self, other):
        results = [
            max(self.a, other.a),
            max(self.a, other.b),
            max(self.b, other.a),
            max(self.b, other.b),
        ]
        return Interval(min(results), max(results))

    @property
    def width(self):
        if math.isnan(self.a) or math.isnan(self.b):
            return 0
        return self.b - self.a

    def contains(self, other):
        """
        Returns either [0, 0], [0, 1], or [1, 1].
        If self fully contains other, returns [1, 1].
        If other lies fully outside self, returns [0, 0].
        Otherwise returns [0, 1]
        """
        if not other:
            return True
        if other.a in self and other.b in self:
            return Interval(1, 1)
        if other.b < self.a or other.a > self.b:
            return Interval(0, 0)
        return Interval(0, 1)
