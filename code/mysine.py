#!/usr/bin/env python3

import argparse
from collections import deque
import time
from typing import Generator, Callable

from vibes import vibes

import contractors
from mybox import Box
from myinterval import Interval, INF, NAN
from contractors import contract_sum, contract_mul, contract_sqr, relative_change

from myring import forward_backward


def parse_args():
    parser = argparse.ArgumentParser("Drawing rings for exercise 8")
    parser.add_argument(
        "-e",
        "--example",
        choices=["rising", "falling", "sin", "solver"],
        default="rising",
        help="What example to draw. Can be either the rising flank of sin, the falling flank, the full sin graph, or the example equation solver involving sin and cos",
    )
    parser.add_argument(
        "-w",
        "--split-threshold",
        type=float,
        default=0.01,
        help=(
            "Boxes with a width smaller than this will not be split anymore and "
            'just be displayed as "unknown"'
        ),
    )
    parser.add_argument(
        "-S",
        "--search-box",
        type=float,
        default=[-10, 10, -10, 10],
        nargs=4,
        help="Bounding box to search in.",
    )
    parser.add_argument(
        "-bf",
        "--breadth-first",
        action="store_true",
        help=(
            "By default, boxes are explored in a depth-first manner. "
            "Setting this switches to a breadth-first strategy."
        ),
    )
    parser.add_argument(
        "-s",
        "--sleep",
        type=float,
        default=0,
        help="Time to pause after each box. Only useful for visual enjoyment.",
    )
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()


def sivia(
    S: Box,
    contractor: Callable,
    min_width=0.01,
    breadth_first=False,
) -> Generator[tuple[Box, str], None, None]:
    queue = deque([S])
    while queue:
        S_current = queue.popleft()
        if S_current.width[0] < min_width:
            continue

        S_foreground = contractor(S_current)
        yield S_current, "background"
        yield S_foreground, "foreground"

        left_half, right_half = S_foreground.split_along_width()
        if breadth_first:
            queue.append(left_half)
            queue.append(right_half)
        else:
            queue.appendleft(left_half)
            queue.appendleft(right_half)


STATE2COLOR = {"background": "[cyan]", "foreground": "[yellow]"}


def contract_solver(S):
    x, y = S.intervals
    x, y = contract_sin_xsqr(x, y)
    x, y = contractors.contract_cos(x, y)
    return Box(x, y)


def contract_sin_xsqr(x, y):
    x2 = Interval.real()
    x, x2 = contractors.contract_sqr(x, x2)
    x2, y = contractors.contract_sin(x2, y)
    x, x2 = contractors.contract_sqr(x, x2)
    return x, y


def main(args):
    vibes.beginDrawing()
    vibes.newFigure("Solution Set")

    # init values taken from exercise video
    S = Box((-10, 10), (-2, 2))
    vibes.setFigureProperties(
        {
            "x": 400,
            "y": 400,
            "width": int(1024 * S[0].width / 20),
            "height": int(1024 * S[1].width / 20),
        }
    )
    vibes.axisLimits(*S.corners)

    if args.example == "rising":
        contractor = contractors.contract_sin_rising
    elif args.example == "falling":
        contractor = contractors.contract_sin_falling
    elif args.example == "sin":
        contractor = contractors.contract_sin
    else:
        contractor = contract_solver

    for box, state in sivia(S, contractor, args.split_threshold, args.breadth_first):
        vibes.drawBox(*box.corners, STATE2COLOR[state])
        if args.sleep:
            time.sleep(args.sleep)


if __name__ == "__main__":
    main(args)
