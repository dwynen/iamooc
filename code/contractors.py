import math
from typing import Union, Callable

import numpy as np

from myinterval import Interval, INF, NAN
from mybox import Box


def relative_change(before, after):
    diff_a = abs(before.a - after.a)
    relative_a = diff_a / (before.a or 1)
    diff_b = abs(before.b - after.b)
    relative_b = diff_b / (before.b or 1)
    return max(relative_a, relative_b)


def contract_sum(x, y, z):
    """
    produces a contraction for the constraint z = x + y
    """
    return x & (z - y), y & (z - x), z & (x + y)


def contract_mul(x, y, z):
    """
    produces a contraction for the costraint z = x * y
    """
    return x & (z / y), y & (z / x), z & (x * y)


def contract_monotonous(x, y, f, f_inv):
    """
    produces a contraction for y = f(x) where f is monotonous
    f_inv must be the inverse function of f
    """
    pass


def contract_sqr(x, y):
    """
    produces a contraction for y = x**2
    """
    left = x & Interval(-INF, 0)
    right = x & Interval(0, INF)
    y = (y & left**2) | (y & right**2)

    # check if this is already empty. if so, must have been empty solution
    # space to begin with -> contract to empty box
    if not y:
        return Interval.empty_interval(), Interval.empty_interval()

    left &= -y.sqrt()
    right &= y.sqrt()
    return left | right, y


def contract_int(x: Interval):
    if not x:
        return Interval.empty_interval()
    return Interval(np.ceil(x.a), np.floor(x.b))


def contract_mod(x: Interval, period: Union[float, Interval]):
    factor = Interval(-INF, INF)
    # period might be an interval, or might just be float
    period = Interval(period)
    period, factor, x = contract_mul(period, factor, x)
    factor = contract_int(factor)
    period, factor, x = contract_mul(period, factor, x)
    return x, period


def contract_periodicity(
    contractor: Callable, x: Interval, y: Interval, period: Union[float, Interval]
):
    z = Interval.real()
    d = Interval.real()
    z, y = contractor(z, y)
    d, x, z = contract_sum(d, x, z)
    # TODO: is it valid to contract the period here?
    d, period = contract_mod(d, period)
    d, x, z = contract_sum(d, x, z)
    z, y = contractor(z, y)
    return x, y, period


def contract_sin_rising(x: Union[Interval, Box], y: Union[None, Interval] = None):
    return_interval = isinstance(x, Interval)
    if return_interval:
        assert isinstance(
            y, Interval
        ), "Need to receive either one Box or two intervals!"
    else:
        assert isinstance(x, Box)
        assert y is None
        x, y = x.intervals
    x &= Interval(-np.pi / 2, np.pi / 2)
    y &= Interval(np.sin(x.a), np.sin(x.b))
    x &= Interval(np.arcsin(y.a), np.arcsin(y.b))
    return (x, y) if return_interval else Box(x, y)


def contract_sin_falling(x: Union[Interval, Box], y: Union[None, Interval] = None):
    return_interval = isinstance(x, Interval)
    if return_interval:
        assert isinstance(
            y, Interval
        ), "Need to receive either one Box or two intervals!"
    else:
        assert isinstance(x, Box)
        assert y is None
        x, y = x.intervals
    # np.pi = 3.141592653589793238462643383279502884
    # which is an underestimate of pi
    # the real value of pi is thus within the interval of np.pi and the
    # number directly following it in the sequence of IEEE floats
    pi = Interval(np.pi, math.nextafter(np.pi, INF))
    x0 = Interval(-np.pi / 2, np.pi / 2)
    x, x0, pi = contract_sum(x, x0, pi)
    x0, y = contract_sin_rising(x0, y)
    x, x0, pi = contract_sum(x, x0, pi)
    return (x, y) if return_interval else Box(x, y)


def contract_sin(x: Union[Interval, Box], y: Union[None, Interval] = None):
    return_interval = isinstance(x, Interval)
    if return_interval:
        assert isinstance(
            y, Interval
        ), "Need to receive either one Box or two intervals!"
    else:
        assert isinstance(x, Box)
        assert y is None
        x, y = x.intervals
    x0, y0, _ = contract_periodicity(contract_sin_rising, x, y, 2 * np.pi)
    x1, y1, _ = contract_periodicity(contract_sin_falling, x, y, 2 * np.pi)
    x = x0 | x1
    y = y0 | y1
    return (x, y) if return_interval else Box(x, y)


def contract_cos(x: Union[Interval, Box], y: Union[None, Interval] = None):
    return_interval = isinstance(x, Interval)
    if return_interval:
        assert isinstance(
            y, Interval
        ), "Need to receive either one Box or two intervals!"
    else:
        assert isinstance(x, Box)
        assert y is None
        x, y = x.intervals
    z = Interval.real()
    pi2 = Interval(np.pi / 2, np.pi / 2)
    x, z, pi2 = contract_sum(x, z, pi2)
    z, y = contract_sin(z, y)
    x, z, pi2 = contract_sum(x, z, pi2)
    return x, y
