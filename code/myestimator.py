#!/usr/bin/env python3

import time
import argparse
from collections import deque

import numpy as np
from vibes import vibes

from mybox import Box
from myinterval import Interval, INF, NAN

PARAM_ESTIMATION = "Parameter estimation"
ENCLOSURE = "Enclosure"


def parse_args():
    parser = argparse.ArgumentParser("Parameter estimator for exercise 4")
    parser.add_argument(
        "-q",
        "--accept-outliers",
        type=int,
        default=0,
        help="The chosen parameters may be in conflict with this many measurements",
    )
    parser.add_argument(
        "-w",
        "--split-threshold",
        type=float,
        default=0.01,
        help=(
            "Boxes with a width smaller than this will not be split anymore and "
            'just be displayed as "unknown"'
        ),
    )
    parser.add_argument(
        "-t",
        "--time-range",
        type=float,
        default=[0, 5, 0.1],
        nargs=3,
        help=(
            "Time range for drawing the function enclosure in "
            "[start, stop, step] format. Note that the stop point will always "
            "be included."
        ),
    )
    parser.add_argument(
        "--enclosure",
        action="store_true",
        help=(
            "If enabled, draw the enclosure of the estimated parameters and "
            "compare them to the measurements"
        ),
    )
    parser.add_argument(
        "-bf",
        "--breadth-first",
        action="store_true",
        help=(
            "By default, boxes are explored in a depth-first manner. "
            "Setting this switches to a breadth-first strategy."
        ),
    )
    parser.add_argument(
        "-s",
        "--sleep",
        type=float,
        default=0,
        help="Time to pause after each box. Only useful for visual enjoyment.",
    )
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()


def sivia(measurements, f, max_box, accept_outliers, split_threshold):
    """
    Computes boxes that form a superset and a subset subpaving of parameters
    that explain the measurements.
    """
    queue = deque([max_box])
    while queue:
        current_box = queue.popleft()
        measurement_tests = sum(
            [
                confidence_interval.contains(f(t, *current_box.intervals))
                for (t, confidence_interval) in measurements
            ]
        )

        # base case: we know that this box is outside enough measurment tests
        # that checking in mor detail is pointless -> invalid
        if len(measurements) - accept_outliers > measurement_tests.b:
            yield current_box, "invalid"
        # base case: we already know that this box satisfies our criterion
        # even if the remaining measurements turn out to be violated -> valid
        elif len(measurements) - accept_outliers <= measurement_tests.a:
            yield current_box, "valid"
        # base case for limiting the computation time: we won't split boxes
        # infinitely -> unknown
        elif current_box.width[0] < split_threshold:
            yield current_box, "unknown"
        # Recursion: split the box along its width, add both resulting boxes to
        # the queue.
        # The two variants here are simply to see how the algorithm works
        # use --sleep to visualize the difference :)
        elif args.breadth_first:
            queue.extend(current_box.split_along_width())
        else:
            queue.extendleft(current_box.split_along_width())


def example_f(t, p1, p2):
    return p1 * (t * p2).exp()


def draw_enclosure(box, time_range, f, color="green[green]"):
    ts = np.arange(*time_range)
    if ts[0] != time_range[1]:
        ts = np.append(ts, [time_range[1]])
    for t1, t2 in zip(ts[:-1], ts[1:]):
        result = f(Interval(t1, t2), *box.intervals)
        vibes.drawBox(t1, t2, result.a, result.b, color)


def main(args):
    box = Box([0, 1], [-2, 2])
    measurements = [
        [0.2, Interval(1.5, 2)],
        [1, Interval(0.7, 0.8)],
        [2, Interval(0.1, 0.3)],
        [4, Interval(-0.1, 0.03)],
    ]
    vibes.beginDrawing()

    state2color = {
        "valid": "[red]",
        "unknown": "[yellow]",
        "invalid": "[blue]",
    }

    search_box = Box((-3, 3), (-3, 3))
    vibes.newFigure(PARAM_ESTIMATION)
    vibes.axisLimits(*search_box.corners)
    vibes.setFigureProperties({"x": 768, "y": 100, "width": 768, "height": 768})

    if args.enclosure:
        vibes.newFigure(ENCLOSURE)
        vibes.setFigureProperties({"x": 0, "y": 0, "width": 768, "height": 768})

    for box, state in sivia(
        measurements,
        example_f,
        search_box,
        accept_outliers=args.accept_outliers,
        split_threshold=args.split_threshold,
    ):
        if args.enclosure and state == "unknown":
            vibes.selectFigure(ENCLOSURE)
            draw_enclosure(box, args.time_range, example_f)
        vibes.selectFigure(PARAM_ESTIMATION)
        vibes.drawBox(*box.corners, state2color[state])
        if args.sleep:
            time.sleep(args.sleep)

    if args.enclosure:
        vibes.selectFigure(ENCLOSURE)
        for (t, x) in measurements:
            vibes.drawBox(
                t - args.time_range[2] / 4,
                t + args.time_range[2] / 4,
                x.a,
                x.b,
                "[orange]",
            )

    vibes.endDrawing()


if __name__ == "__main__":
    main(args)
