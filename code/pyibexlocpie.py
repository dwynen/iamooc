#!/usr/bin/env python3

import argparse

import pyibex as ibex
from pyibex.geometry import SepPolarXY
from vibes import vibes
import numpy as np

# 🦇 🦇 🦇 pretty colors 🦇 🦇 🦇
import dracula
from pyibexestimator import gomne


def parse_args():
    parser = argparse.ArgumentParser("Location estimation from known landmarks")
    parser.add_argument(
        "-i",
        "--measurement",
        type=int,
        help="If given, only draw the solution set for this one measurement",
    )
    parser.add_argument(
        "-q",
        "--accept-outliers",
        type=int,
        help="If given, the chosen number of measurements may be in conflict with the parameters. Does not combine with -q.",
    )
    parser.add_argument(
        "-w",
        "--split-threshold",
        type=float,
        default=0.01,
        help=(
            "Boxes with a width smaller than this will not be split anymore and "
            'just be displayed as "unknown"'
        ),
    )
    parser.add_argument(
        "-g",
        "--gomne",
        action="store_true",
        help="If given, run GOMNE for each input measurement, checking whether it should be regarded as an outlier.",
    )

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()


def main(args):
    LANDMARKS = [[6, 12], [-2, -5], [-3, 10], [3, 4]]
    DISTANCES = [[10, 13], [8, 10], [5, 7], [6, 8]]
    ALPHA = [[0.5, 1], [-3, -1.5], [1, 2], [2, 3]]

    X0 = ibex.IntervalVector([[-20, 20], [-20, 20]])

    vibes.beginDrawing()
    vibes.newFigure("Spot the robot")

    vibes.setFigureProperties({"x": 200, "y": 100, "width": 768, "height": 768})
    vibes.axisEqual()
    vibes.drawBox(*corners(4 * X0), color=f"[{dracula.BACKGROUND}]")
    vibes.axisLimits(*corners(X0))

    separators = list()
    for (lm, d, alpha) in zip(LANDMARKS, DISTANCES, ALPHA):
        pie_separator = ibex.geometry.SepPolarXY(ibex.Interval(*d), ibex.Interval(*alpha))
        transform_function = ibex.Function("v1", "v2", f"({lm[0]} - v1; {lm[1]} - v2)")
        transform_inverse = ibex.Function("p1", "p2", f"({lm[0]} - p1; {lm[1]} - p2)")
        transformed_separator = ibex.SepTransform(
            pie_separator,
            transform_function,
            transform_inverse,
        )
        separators.append(transformed_separator)

    if args.measurement is not None:
        separator = separators[args.measurement]
        assert (
            not args.gomne and args.accept_outliers is None
        ), "use -q, -i, and --g exclusively"
    else:
        separator = ibex.SepQInterProjF(separators)
        if args.accept_outliers is not None:
            assert not args.gomne, "use -q, -i, and --g exclusively"
            separator.q = args.accept_outliers
        else:
            separator = ibex.SepQInterProjF(separators)
            args.accept_outliers, args.split_threshold = gomne(
                separator, X0, args.split_threshold, len(LANDMARKS)
            )
            s = s if args.outliers > 1 else ""
            print(f"Seems we have {args.accept_outliers} outlier{s}.")

    ibex.pySIVIA(
        X0,
        separator,
        color_maybe=f"{dracula.CURRENT}[{dracula.GREEN}]",
        color_in=f"{dracula.CURRENT}[{dracula.PURPLE}]",
        color_out=f"{dracula.CURRENT}",
        epsilon=args.split_threshold,
    )

    # draw the true landmark positions on top of everything else for reference
    for l in LANDMARKS:
        vibes.drawCircle(l[0], l[1], 0.8, color=f"{dracula.RED}[{dracula.RED}aa]")


def corners(box: ibex.IntervalVector) -> tuple[float, float, float, float]:
    return box[0].lb(), box[0].ub(), box[1].lb(), box[1].ub()


if __name__ == "__main__":
    main(args)
