#!/usr/bin/env python3

import argparse
from collections import deque
import time
from typing import Generator

from vibes import vibes

from mybox import Box
from myinterval import Interval, INF, NAN
from contractors import contract_sum, contract_mul, contract_sqr, relative_change

from myring import forward_backward


def parse_args():
    parser = argparse.ArgumentParser("Drawing rings for exercise 7")
    parser.add_argument(
        "-e",
        "--example",
        choices=["single", "intersection", "union"],
        default="single",
        help="What example to draw",
    )
    parser.add_argument(
        "-w",
        "--split-threshold",
        type=float,
        default=0.01,
        help=(
            "Boxes with a width smaller than this will not be split anymore and "
            'just be displayed as "unknown"'
        ),
    )
    parser.add_argument(
        "-S",
        "--search-box",
        type=float,
        default=[-10, 10, -10, 10],
        nargs=4,
        help="Bounding box to search in.",
    )
    parser.add_argument(
        "-bf",
        "--breadth-first",
        action="store_true",
        help=(
            "By default, boxes are explored in a depth-first manner. "
            "Setting this switches to a breadth-first strategy."
        ),
    )
    parser.add_argument(
        "-s",
        "--sleep",
        type=float,
        default=0,
        help="Time to pause after each box. Only useful for visual enjoyment.",
    )
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()


def ring_separator(
    S: Box, C: Box, r: Interval, complement: bool
) -> tuple[Box, Box, Interval]:
    """
    Contracts S to either the ring C,r or its complement.
    """
    if not complement:
        # nothing special to do here
        return forward_backward(S, C, r)

    S_inner = forward_backward(S, C, Interval(-1, r.a))
    S_outer = forward_backward(S, C, Interval(r.b, INF))
    return S_inner | S_outer


def ring(
    S: Box,
    C: Box,
    r: Interval,
    min_width=0.01,
) -> Generator[tuple[Box, str], None, None]:
    queue = deque([S])
    while queue:
        S_current = queue.popleft()
        if S_current.width[0] < min_width:
            continue
        S_complement = ring_separator(S_current, C, r, complement=True)
        S_limit = ring_separator(S_complement, C, r, complement=False)

        yield S_current, "inside"
        yield S_complement, "outside"
        yield S_limit, "limit"

        left_half, right_half = S_limit.split_along_width()
        if args.breadth_first:
            queue.append(left_half)
            queue.append(right_half)
        else:
            queue.appendleft(left_half)
            queue.appendleft(right_half)


def two_rings(
    S: Box,
    Ca: Box,
    Cb: Box,
    ra: Interval,
    rb: Interval,
    op: str,
    min_width=0.1,
) -> Generator[tuple[Box, str], None, None]:
    queue = deque([S])
    while queue:
        S_current = queue.popleft()
        if S_current.width[0] < min_width:
            continue
        S_complement_a = ring_separator(S_current, Ca, ra, complement=True)
        S_complement_b = ring_separator(S_current, Cb, rb, complement=True)

        S_complement = (
            S_complement_a | S_complement_b
            if op == "intersection"
            else S_complement_a & S_complement_b
        )

        S_limit_a = ring_separator(S_complement, Ca, ra, complement=False)
        S_limit_b = ring_separator(S_complement, Cb, rb, complement=False)
        S_limit = (
            S_limit_a & S_limit_b if op == "intersection" else S_limit_a | S_limit_b
        )

        yield S_current, "inside"
        yield S_complement, "outside"
        yield S_limit, "limit"

        left_half, right_half = S_limit.split_along_width()
        if args.breadth_first:
            queue.append(left_half)
            queue.append(right_half)
        else:
            queue.appendleft(left_half)
            queue.appendleft(right_half)

        if args.sleep:
            time.sleep(args.sleep)


STATE2COLOR = {"outside": "black[cyan]", "limit": "[yellow]", "inside": "red[magenta]"}


def main(args):
    vibes.beginDrawing()
    vibes.newFigure("Solution Set")
    vibes.setFigureProperties({"x": 200, "y": 200, "width": 768, "height": 768})

    # init values taken from exercise video
    S = Box((-10, 10), (-10, 10))
    vibes.axisLimits(*S.corners)

    # init values taken from exercise text
    Ca = Box((1, 1), (2, 2))
    Cb = Box((2, 2), (5, 5))
    ra = Interval(4, 5)
    rb = Interval(5, 6)

    if args.example == "single":
        generator = ring(S, Ca, ra, min_width=args.split_threshold)
    else:
        generator = two_rings(
            S, Ca, Cb, ra, rb, min_width=args.split_threshold, op=args.example
        )

    for box, state in generator:
        vibes.drawBox(*box.corners, STATE2COLOR[state])


if __name__ == "__main__":
    main(args)
