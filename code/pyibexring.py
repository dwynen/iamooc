#!/usr/bin/env python3

import argparse

import pyibex as ibex
from vibes import vibes

# 🦇 🦇 🦇 pretty colors 🦇 🦇 🦇
import dracula


def _make_interval(values):
    return ibex.Interval(*map(float, values))


def _make_box(values):
    values = [float(a) for a in values]
    values = [[t0, t1] for (t0, t1) in zip(values[0::2], values[1::2])]
    return ibex.IntervalVector(values)


def parse_args():
    parser = argparse.ArgumentParser("Exercise no. 8 from the IAMOOC")
    parser.add_argument(
        "-ex",
        "--example",
        choices=[
            "simple",
            "ctcfwdbwd",
            "intersection",
            "union",
        ],
        default="simple",
        help="which part of the exercise to demo",
    )
    parser.add_argument(
        "-op",
        "--operator",
        choices=[
            "contractor",
            "separator",
        ],
        default="contractor",
        help="for the ctcfwdbwd example, whether to use a contractor or a separator",
    )
    parser.add_argument(
        "-Ix",
        type=float,
        nargs=2,
        default=(1, 3),
        help="Position of the ring in the x direction",
    )
    parser.add_argument(
        "-Iy",
        type=float,
        nargs=2,
        default=(2, 4),
        help="Position of the ring in the y direction",
    )
    parser.add_argument(
        "-Ir",
        type=float,
        nargs=2,
        default=(5, 6),
        help="Inner and outer radius of the ring. Keeping default from last exercise.",
    )
    parser.add_argument(
        "-C1",
        type=float,
        nargs=2,
        default=(1, 2),
        help="x and y position (not intervals!) of the first circle for parts 3) and 4).",
    )
    parser.add_argument(
        "-C2",
        type=float,
        nargs=2,
        default=(2, 5),
        help="x and y position (not intervals!) of the second circle for parts 3) and 4).",
    )
    parser.add_argument(
        "-Ir1",
        type=float,
        nargs=2,
        default=(4, 5),
        help="Inner and outer radius of the ring first ring for 3) and 4).",
    )
    parser.add_argument(
        "-Ir2",
        type=float,
        nargs=2,
        default=(5, 6),
        help="Inner and outer radius of the ring second ring for 3) and 4).",
    )
    parser.add_argument(
        "-X0",
        type=float,
        nargs=4,
        default=(-10, 10, -10, 10),
        help="Outer bounding box",
    )
    parser.add_argument(
        "-w",
        type=float,
        default=0.1,
        help="Threshold at which to stop splitting boxes",
    )

    args = parser.parse_args()
    args.X0 = _make_box(args.X0)
    args.Ix = _make_interval(args.Ix)
    args.Iy = _make_interval(args.Iy)
    args.Ir = _make_interval(args.Ir)
    args.Ir = _make_interval(args.Ir)
    args.Ir1 = _make_interval(args.Ir1)
    args.Ir2 = _make_interval(args.Ir2)
    return args


if __name__ == "__main__":
    args = parse_args()


class SimpleContractor(ibex.Ctc):
    def __init__(self, C, Ir):
        ibex.Ctc.__init__(self, 2)
        self.C = C
        self.radius = Ir

    def contract(self, X):
        c0 = ibex.Interval(self.C[0])
        c1 = ibex.Interval(self.C[1])
        a0 = X[0] - c0
        a1 = X[1] - c1
        a0sqr = ibex.sqr(a0)
        a1sqr = ibex.sqr(a1)
        r2 = ibex.sqr(self.radius)

        A01sqr = a0sqr + a1sqr

        # TODO: should this happen?
        A01sqr &= r2

        ibex.bwd_add(A01sqr, a0sqr, a1sqr)
        ibex.bwd_sqr(a0sqr, a0)
        ibex.bwd_sqr(a1sqr, a1)
        ibex.bwd_sub(a0, X[0], c0)
        ibex.bwd_sub(a1, X[1], c1)


def simple(X0, Ix, Iy, Ir):
    C = ibex.IntervalVector([Ix, Iy])
    contractor = SimpleContractor(C, Ir)
    return contractor


def ctcfwdbwd(X0, Ix, Iy, Ir, op="contractor"):
    f_str = f"(x1 - {Ix})^2 + (x2 - {Iy})^2"
    f = ibex.Function("x1", "x2", f_str)
    if op == "contractor":
        operator = ibex.CtcFwdBwd(f, ibex.sqr(Ir))
    else:
        assert op == "separator", op
        operator = ibex.SepFwdBwd(f, ibex.sqr(Ir))
    return separator


def combination(X0, C1, Ir1, C2, Ir2, operator):
    ring_1_str = f"(x1 - {C1[0]})^2 + (x2 - {C1[1]})^2"
    ring_2_str = f"(x1 - {C2[0]})^2 + (x2 - {C2[1]})^2"
    ring_1 = ibex.Function("x1", "x2", ring_1_str)
    ring_2 = ibex.Function("x1", "x2", ring_2_str)

    separator_1 = ibex.SepFwdBwd(ring_1, ibex.sqr(Ir1))
    separator_2 = ibex.SepFwdBwd(ring_2, ibex.sqr(Ir2))
    if operator == "union":
        separator = separator_1 | separator_2
    else:
        separator = separator_1 & separator_2
    return separator


def main(args):
    vibes.beginDrawing()
    vibes.newFigure(f"Example: {args.example}")
    vibes.setFigureProperties({"x": 200, "y": 200, "width": 768, "height": 768})
    vibes.axisEqual()
    vibes.drawBox(*corners(4 * args.X0), color=f"[{dracula.BACKGROUND}]")
    vibes.axisLimits(args.X0[0].lb(), args.X0[0].ub(), args.X0[1].lb(), args.X0[1].ub())

    if args.example == "simple":
        # not really a separator but okay
        separator = simple(args.X0, args.Ix, args.Iy, args.Ir)
    elif args.example == "ctcfwdbwd":
        separator = ctcfwdbwd(args.X0, args.Ix, args.Iy, args.Ir, args.operator)
    else:
        separator = combination(
            args.X0, args.C1, args.Ir1, args.C2, args.Ir2, args.example
        )

    ibex.pySIVIA(
        args.X0,
        separator,
        args.w,
        color_maybe=f"{dracula.CURRENT}[{dracula.GREEN}]",
        color_in=f"{dracula.CURRENT}[{dracula.PURPLE}]",
        color_out=f"{dracula.CURRENT}",
    )


def corners(box: ibex.IntervalVector) -> tuple[float, float, float, float]:
    return box[0].lb(), box[0].ub(), box[1].lb(), box[1].ub()


if __name__ == "__main__":
    main(args)
