#!/usr/bin/env python3

from typing import Generator

from vibes import vibes

from mybox import Box
from myinterval import Interval, INF, NAN
from contractors import contract_sum, contract_mul, contract_sqr, relative_change


def forward_backward(S: Box, C: Box, r: Interval) -> tuple[Box, Box, Interval]:
    """
    Contracts the solution space c with the constraint
    (S1 - C1)**2 + (S2 - C2)**2 == r**2
    """
    # forward pass:
    a = S[0] - C[0]
    b = S[1] - C[1]
    c = a**2
    d = b**2
    e = r**2

    # backwards pass
    c, d, e = contract_sum(c, d, e)
    r, e = contract_sqr(r, e)
    a, c = contract_sqr(a, c)
    b, d = contract_sqr(b, d)
    C0, a, S0 = contract_sum(C[0], a, S[0])
    C1, a, S1 = contract_sum(C[1], b, S[1])
    return Box(S0, S1)


def pave_superset(
    S: Box, C: Box, r: Interval, min_width=0.1
) -> Generator[tuple[Box, str], None, None]:
    # base case: we have explored long enough, don't generate more boxes
    if S.width[0] < min_width:
        return

    # contract the current box S
    # TODO: can we also contract C and r here?
    yield S, "invalid"
    S = forward_backward(S, C, r)
    yield S, "maybe"

    # recurse into the halves of the contracted box:
    left_half, right_half = S.split_along_width()
    yield from pave_superset(left_half, C, r)
    yield from pave_superset(right_half, C, r)


STATE2COLOR = {
    "invalid": "[cyan]",
    "maybe": "[yellow]",
}


def main():
    vibes.beginDrawing()
    vibes.newFigure("Solution Set")
    vibes.setFigureProperties({"x": 0, "y": 0, "width": 768, "height": 768})

    # init values taken from exercise video
    S = Box((-10, 10), (-10, 10))
    vibes.axisLimits(*S.corners)

    # init values taken from exercise text
    C = Box((1, 3), (2, 4))
    r = Interval(4, 5)

    for box, state in pave_superset(S, C, r):
        vibes.drawBox(*box.corners, STATE2COLOR[state])


if __name__ == "__main__":
    main()
