# IAMOOC Sprint 2022 Coursework

These scripts were written following the exercises presented in Luc Jaulin's
Interval Analysis MOOC.
For more information, visit the [course website][moocweb].

![subpaving.png](iamooc.png)



[moocweb]: https://www.ensta-bretagne.fr/jaulin/iamooc.html
